package com.laymanlearn.springbootredis.controller;

import com.laymanlearn.springbootredis.model.User;
import com.laymanlearn.springbootredis.service.UserService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author: Damodar Naidu M
 * @Year: 2023
 */
@Slf4j
@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/{userId}")
    public User getUserAddress(@PathVariable(value = "userId") String userId){
        return service.getUserById(userId);
    }
}
