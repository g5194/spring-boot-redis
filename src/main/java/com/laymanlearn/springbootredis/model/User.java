package com.laymanlearn.springbootredis.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: Damodar Naidu M
 * @Year: 2023
 */
@Setter
@Getter
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private String city;
}
