package com.laymanlearn.springbootredis.service;

import com.laymanlearn.springbootredis.model.User;


/**
 * @Author: Damodar Naidu M
 * @Year: 2023
 */
public interface UserService {
    User getUserById(String addressId);
}
