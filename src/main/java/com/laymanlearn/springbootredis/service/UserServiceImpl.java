package com.laymanlearn.springbootredis.service;

import com.laymanlearn.springbootredis.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Damodar Naidu M
 * @Year: 2023
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService{

    private Map<String, User> userMockData = new HashMap<>();

    public UserServiceImpl(){
        prepareUserData();
    }

    private void prepareUserData(){
        userMockData.put("user1", new User("user1", "User 1", "Blr"));
        userMockData.put("user2", new User("user2", "User 2", "Hyd"));
        userMockData.put("user3", new User("user3", "User 3", "Chennai"));
        userMockData.put("user4", new User("user4", "User 4", "Blr"));
    }

    @Override
    public User getUserById(String userId) {
        // code to fetch item from database
        log.info("Get user details by userId {}", userId);
        return getUser(userId);
    }

    @Cacheable(value = "user", key = "#userId")
    public User getUser(String userId){
        log.info("Get user details by by cached method and userId {}", userId);
        return userMockData.get(userId);
    }

}
